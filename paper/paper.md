---
title: 'georg: Towards an Automated Data Pipeline for the Identification of Geographically Proximal and Pharmaceutically Relevant Compounds'
tags:
  - Python
  - data-engineering
  - pre-processing f. digital screening
authors:
  - name: Daniel 'grindhold' Brendle
    orcid: 0000-0003-2451-7170
  - name: Regina Erhardt
date: 04 July 2020
bibliography: paper.bib
---

Introduction
============

The worldwide spread of SARS-CoV-2 in early 2020 and the following (and
at the time of writing ongoing) research for effective medication and
vaccination along with the measurements taken by governments and their
implications on the broader public underline the importance of
responding to a novel virus with an appropriate treatment as soon as
possible. The usual path to develop medication against a novel pathogen
involves analyzing the pathogen by means of DNA sequencing and X-ray
crystallography and thereupon developing molecules that are likely to
interact with the pathogen in the desired way. In case of successful
testing the novel compound then has to undergo long-running procedures
of approval by public authorities. [@drug_discovery]\
Before the invention of synthetic drugs, humankind --- pharmacy-wise ---
relied entirely on compounds produced in plants and animals. Being able
to respond to a novel pathogen with one of these compounds would offer
significant benefits: Many of them are well-studied, thus many already
have authoritative approval as medication in several jurisdictions which
raises the odds that such medication could be rolled out faster. As
these organisms produce the compounds naturally, there is no need for
complicated syntheses as opposed to e.g. the synthesis of Remdesivir
[@remdesivir]. They merely have to be extracted. Additionally, something
that grows in their respective surrounding environment is --- to a
certain extent --- directly available to the population, independent of
any industrial production process.\
There is no publically availabe system capable of deriving a set of
compounds likely to be found in the surroundings of a geographic
coordinate. It shall be assessed in this paper whether such a system is
feasible.

Pipeline Design
===============

[\[fig:abstract\_pipeline\]]{#fig:abstract_pipeline
label="fig:abstract_pipeline"}

The abstract configuration of this pipeline can be seen in
Figure [\[fig:abstract\_pipeline\]](#fig:abstract_pipeline){reference-type="ref"
reference="fig:abstract_pipeline"}. To simplify the proof-of-concept the
geographical scope of the pipeline described in this paper is limited to
Europe, however, when a globally applicable solution to a problem can be
deployed without causing overhead, that solution shall be chosen. Due to
it's ease of use, broad support in the data-science community and
resulting abundance of useful third-party modules, all code related to
this paper is written in Python 3.

Resolving geocoordinates to species
-----------------------------------

In the pursuit to implement this pipeline [^1], sources must be found
that provide the wanted data. The optimal form of data would be
2D-pointclouds / heatmaps representing sightings that are linked to
species. No such dataset could yet be obtained in sufficient quality. At
the cost of a certain loss of precision of resulting predictions, the
solution is replacing such a dataset by combining three distinct
datasets released by the European Environment Agency (EEA). The EEA
defines geobiological [@biogeodata] and marinebiological
[@marinegeodata] zones inside Europe to facilitate a reference framework
for their own assessments. Additionally, they provide a database that
maps these zones to species [@speciesgeodb].

![image](geobio.jpg){width="\textwidth"}
[\[fig:biogeo\_regions\]]{#fig:biogeo_regions
label="fig:biogeo_regions"}

The geographic data must be converted into a form that makes it easy to
execute queries against. Inspecting these datasets leads to the
conclusion that the data formats, as presented by the EEA, are not fully
interoperable by default. Each datasource encodes their biogeographical
zones by using different forms of string identifiers. The marine map
subdivides its regions with a granularity that is too fine for the
species database to resolve. This problem can be mitigated by providing
a table of translations to convert between the different formats.

  Map   Long Identifier                         Abbreviation
  ----- --------------------------------------- --------------
        alpine                                  ALP
        anatolian                               ANA
        arctic                                  ARC
        atlantic                                ATL
        blackSea                                BLS
        boreal                                  BOR
        continental                             CON
        macaronesia                             MAC
        mediterranean                           MED
        outside                                 OUT
        pannonian                               PAN
        steppic                                 STE
        North-east Atlantic Ocean               MATL
        Greater North Sea, incl. the Kattegat   MATL
        and the English Channel                 
        Celtic Seas                             MATL
        Macaronesia                             MMAC
        Baltic Sea                              MBAL
        Inonian Sea and the Central             MMED
        Mediterran Sea                          
        Western Mediterranean Sea               MMED
        Adriatic Sea                            MMED
        Aegean-Levantine Sea                    MMED
        Black Sea                               MBLS

[\[fig:zone\_translation\]]{#fig:zone_translation
label="fig:zone_translation"}

In order to facilitate simple geospatial queries, the maps, which ship
in the proprietary shapefile format, are being converted to the free and
open GeoJSON format using QGIS. This conversion step is also being used
to ensure that all maps are converted to the EPSG:4326 coordinate system
which is also the base of GPS coordinates, already widely adopted and
thus the optimal choice to provide a highly interoperable system.
GeoJSON can be parsed by the Python library `fiona` [@fiona]. The
in-memory format of `fiona` can then be processed by `shapely`
[@shapely], another Python library that provides functions to perform a
variety of mathematical operations on geometries. With all these
requirements in place, the pipeline is able to resolve a GPS-coordinate
to a set of geobiological zones by creating a small buffer around the
given coordinate, intersecting it with the geometries of each available
zone and returning each zone that produced an intersectional match.\
The final step is resolving the resulting zones to a list of species.
The aforementioned MAES species database provides a set of CSV files,
among which `species_maes.csv` provides the desired mapping. A lookup
can be facilitated by loading the file via Python's internal `csv`
module, iterating over the content and returning every line that
matches.\

Resolving Species to Compounds
------------------------------

As a means of converting species to compounds, the pipeline uses Dr.
Duke's phytochemical and ethnobotanical databases [@dukedatabase].
Technically these databases come as a set of CSV files, three of which
are relevant to solve the underlying mapping problem. The file
`FNFTAX.csv` contains a list of species along with an unique-per-species
numerical ID called `FNFNUM`. The files `FARMACY.csv` and
`FARMACY_NEW.csv` provide a mapping to substances by referencing the
aforementioned IDs. Making the lookup functionality operational is
effectively parsing these files, scanning `FNFTAX.csv` for a match in
the `TAXON`-field and returning the `FNFNUM` of that particular line. In
a similar fashion, `FARMACY.csv` and `FARMACY_NEW.csv` can be scanned in
order to accumulate any entries that have the previously obtained numer
as their `FNFNUM`. Finally the resulting list must be deduplicated.

Resolving Compound Names to Structures
--------------------------------------

It is very likely that the next step in the pipeline involves
cheminformatical methods like docking simulations or molecular dynamics
simulations. These can be easily automated and require little to no
human interaction. However these methods do not operate on names of
compounds but rather on their threedimensional structure, charges, atom
types, e.t.c. In order to feed these simulations the pipeline must be
able to convert a compound name into a digital representation of the
structure of the molecule. Chemical compounds are textually represented
by either a trivial name (e.g. *Hydroquinone*) or a IUPAC-name (e.g
*Benzene-1,4-diol*).\
IUPAC-nomenclature can be considered a declarative language, designed to
allow the full reconstruction of the molecular topology from a name. In
practice, the package *Open Parser for Systematic IUPAC nomenclature*
(OPSIN) [@opsin] provides that functionality. As OPSIN itself only
provides topological information, additional measures have to be taken
in order to obtain structural information. The (CML-)output of a
finished OPSIN process is a list of atoms and their respective bonds but
the position of every atom is set to (0,0,0). This hinders energetic
forcefield algorithms such as MMFF94
[@mmff94-1; @mmff94-2; @mmff94-3; @mmff94-4; @mmff94-5] to operate
correctly, as every atom being in the same spot results in a net-force
of 0 on each atom. The pipeline therefore adds `cmljitter` [^2] as a
step to randomize the atom positions before applying any means of
energetic relaxation.\
Previous undocumented experiences working with OPSIN and the limitations
of it's functionality (e.g. handling E,Z-isomery and polymers), make it
a reasonable choice to add a different way to retrieve chemical
structures by direct lookups in the Pubchem [@pubchem] database. Pubchem
is an extensive list of SDF-files that contain two- or threedimensional
structures along with several variants of IUPAC names that can be
scanned for matches. The final step of conversion, as far as this
pipeline is concerned, is creating conformers for simulations by using
*OpenBabel*'s [@obabel] `obminimize` tool.

[\[fig:prototype\_pipeline\]]{#fig:prototype_pipeline
label="fig:prototype_pipeline"}

Evaluation
==========

To test the pipeline, the point `52.4597, 13.2193` which is an arbitrary
GPS coordinate inside Berlin, Germany, is being queried. It should
result in the pipeline querying the `CON` geobiological zone whichstands
for \"Continental\" (see
Figure [\[fig:zone\_translation\]](#fig:zone_translation){reference-type="ref"
reference="fig:zone_translation"}). The query results in a list of 177
compounds. This number is unrealistically low. In order to identify the
shortcomings that cause this problems the results of each step are being
analyzed for anomalies.\
The list of species derived from the `CON` zone, is only comprised of
1891 distinct species. This number should be higher. Querying the list
for *Urtica dioica*, the common nettle, which should be found across the
entire european continent shows that it is not contained. This suggests
that the species database is either incomplete or focuses on species
that are of special interest to the authorities. Doing this research in
detail was postponed due to priorites. Manually adding *Urtica dioica*
to the list raises the number of results to 253.\
When inspecting the resulting list of compound, it comes to attention
that many of the entries are simple elements which are not as
interesting for pharmaceutical purposes as organic molecules. These
could safely be blacklisted in a future iteration.\
Considering the origins of the datasources, it does not seem unlikely
that Dr. Dukes databases and MAES Species database do not produce the
best intersectional data due to their different geographic origins. The
U.S. Department of Agriculture is likely more concerned with plants
native to North America than plants native to Europe.\
This section shall be concluded by a list of the number of found
compounds mapped to their respective geobiological zones to provide a
base to measure improvements made to the pipeline in the future:

  Zone   Number of Compounds   
  ------ --------------------- --
  ALP    177                   
  ANA    0                     
  ARC    0                     
  ATL    177                   
  BLS    41                    
  BOR    76                    
  CON    177                   
  MAC    0                     
  MED    223                   
  OUT    0                     
  PAN    41                    
  STE    41                    
  MATL   0                     
  MMAC   0                     
  MBAL   0                     
  MMED   0                     
  MBLS   0                     

[\[fig:zone\_query\]]{#fig:zone_query label="fig:zone_query"}

Conclusion
==========

This work has proven that a lookup mechanism that resolves geographic
coordinates into chemical compound information is feasible but it has
also shown that in order for this to work reliably, more data sources
must be acquired and existing datasources must be made more
comprehensive. In upcoming work I plan to discuss prefiltering and
feeding the output to automated docking simulations.

[^1]: as defined in https://en.wikipedia.org/wiki/Pipeline\_(computing)

[^2]: https://notabug.org/grindhold/cmljitter
