\documentclass[a4paper,9pt,journal]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{multirow}
\usepackage{caption}
\usepackage[pdf]{graphviz}
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{paper.bib}

\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

\begin{document}
\title{Towards an Automated Data Pipeline for the Identification of Geographically Proximal and Pharmaceutically Relevant Compounds using Geobiological and Phytochemical Databases}
\author{Daniel 'grindhold' Brendle}
\date{Hidden University, Institute for Apocalyptic Thinking \\ \today}


\IEEEtitleabstractindextext{%
\begin{abstract}
This paper describes a data-driven approach to derive lists of chemical compounds that are likely to be found in the surroundings of a given geographic coordinate. It also describes measures taken to deliver the compounds in a format that can be easily used by other programs concerned with computational chemistry and biophysics. The practical feasibility of this approach is shown by proof of concept. 
\end{abstract}
\begin{IEEEkeywords}
Data-Engineering, Digital Screening
\end{IEEEkeywords}}

% make the title area
\maketitle
\IEEEdisplaynontitleabstractindextext

\section{Introduction}
The worldwide spread of SARS-CoV-2 in early 2020 and the following (and at the time of writing ongoing) research for effective medication and vaccination along with the measurements taken by governments and their implications on the broader public underline the importance of responding to a novel virus with an appropriate treatment as soon as possible. The usual path to develop medication against a novel pathogen involves analyzing the pathogen by means of DNA sequencing and X-ray crystallography and thereupon developing molecules that are likely to interact with the pathogen in the desired way. In case of successful testing the novel compound then has to undergo long-running procedures of approval by public authorities. \cite{drug_discovery}\\
Before the invention of synthetic drugs, humankind --- pharmacy-wise --- relied entirely on compounds produced in plants and animals. Being able to respond to a novel pathogen with one of these compounds would offer significant benefits: Many of them are well-studied, thus many already have authoritative approval as medication in several jurisdictions which raises the odds that such medication could be rolled out faster. As these organisms produce the compounds naturally, there is no need for complicated syntheses as opposed to e.g. the synthesis of Remdesivir \cite{remdesivir}. They merely have to be extracted. Additionally, something that grows in their respective surrounding environment is --- to a certain extent --- directly available to the population, independent of any industrial production process. \\
There is no publically availabe system capable of deriving a set of compounds likely to be found in the surroundings of a geographic coordinate. It shall be assessed in this paper whether such a system is feasible.
\section{Pipeline Design}
\begin{Figure}
    \centering
    \digraph{ao}{
        rankdir=TB;
        geo [label="Geographical Data" shape=box fontsize=6]
        bio [label="Biological Data" shape=box fontsize=6]
        chem [label="Chemical Data" shape=box fontsize=6]
        geo->bio 
        bio->chem
    }
    \captionof{figure}{Abstract Pipeline Structure}
    \label{fig:abstract_pipeline}
\end{Figure}
The abstract configuration of this pipeline can be seen in  Figure~\ref{fig:abstract_pipeline}. To simplify the proof-of-concept the geographical scope of the pipeline described in this paper is limited to Europe, however, when a globally applicable solution to a problem can be deployed without causing overhead, that solution shall be chosen. Due to it's ease of use, broad support in the data-science community and resulting abundance of useful third-party modules, all code related to this paper is written in Python 3.
\subsection{Resolving geocoordinates to  species}
In the pursuit to implement this pipeline \footnote{as defined in https://en.wikipedia.org/wiki/Pipeline\_(computing)}, sources must be found that provide the wanted data. The optimal form of data would be 2D-pointclouds / heatmaps representing sightings that are linked to species. No such dataset could yet be obtained in sufficient quality. At the cost of a certain loss of precision of resulting predictions, the solution is replacing such a dataset by combining three distinct datasets released by the European Environment Agency (EEA). The EEA defines geobiological \cite{biogeodata} and marinebiological \cite{marinegeodata} zones inside Europe to facilitate a reference framework for their own assessments. Additionally, they provide a database that maps these zones to species \cite{speciesgeodb}. 
\begin{Figure}
    \centering
    \includegraphics[width=\textwidth]{geobio.jpg}
    \captionof{figure}{Geobiological Regions according to EEA \cite{biogeomapimage}}
    \label{fig:biogeo_regions}
\end{Figure}
The geographic data must be converted into a form that makes it easy to execute queries against. 
Inspecting these datasets leads to the conclusion that the data formats, as presented by the EEA, are not fully interoperable by default. Each datasource encodes their biogeographical zones by using different forms of string identifiers. The marine map subdivides its regions with a granularity that is too fine for the species database to resolve. This problem can be mitigated by providing a table of translations to convert between the different formats.
\begin{Figure}
    \centering
    \begin{tabular}{|l|l|l|}
    \hline
    Map & Long Identifier & Abbreviation \\
    \hline
    \multirow{12}{*}{Land} & alpine & ALP \\
        & anatolian & ANA \\
        & arctic & ARC \\
        & atlantic & ATL \\
        & blackSea & BLS \\
        & boreal & BOR \\
        & continental & CON \\
        & macaronesia & MAC \\
        & mediterranean & MED \\
        & outside & OUT \\
        & pannonian & PAN \\
        & steppic & STE \\
    \hline
    \multirow{3}{*}{Marine} & North-east Atlantic Ocean & MATL \\
        & Greater North Sea, incl. the Kattegat & MATL \\ 
        & \hspace{1cm}and the English Channel & \\
        & Celtic Seas & MATL \\
        & Macaronesia & MMAC \\
        & Baltic Sea & MBAL \\
        & Inonian Sea and the Central &  MMED \\
        & \hspace{1cm}Mediterran Sea & \\
        & Western Mediterranean Sea & MMED \\
        & Adriatic Sea & MMED \\
        & Aegean-Levantine Sea & MMED \\
        & Black Sea & MBLS \\
    \hline
    \end{tabular}
    \captionof{figure}{Geobiological Zone Identifiers translation table}
    \label{fig:zone_translation}
\end{Figure}
In order to facilitate simple geospatial queries, the maps, which ship in the proprietary shapefile format, are being converted to the free and open GeoJSON format using QGIS. This conversion step is also being used to ensure that all maps are converted to the EPSG:4326 coordinate system which is also the base of GPS coordinates, already widely adopted and thus the optimal choice to provide a highly interoperable system. GeoJSON can be parsed by the Python library \texttt{fiona} \cite{fiona}. The in-memory format of \texttt{fiona} can then be processed by \texttt{shapely} \cite{shapely}, another Python library that provides functions to perform a variety of mathematical operations on geometries. With all these requirements in place, the pipeline is able to resolve a GPS-coordinate to a set of geobiological zones by creating a small buffer around the given coordinate, intersecting it with the geometries of each available zone and returning each zone that produced an intersectional match. \\
The final step is resolving the resulting zones to a list of species. The aforementioned MAES species database provides a set of CSV files, among which \texttt{species\_maes.csv} provides the desired mapping. A lookup can be facilitated by loading the file via Python's internal \texttt{csv} module, iterating over the content and returning every line that matches. \\
\subsection{Resolving Species to Compounds}
As a means of converting species to compounds, the pipeline uses Dr. Duke's phytochemical and ethnobotanical databases \cite{dukedatabase}. Technically these databases come as a set of CSV files, three of which are relevant to solve the underlying mapping problem. The file \texttt{FNFTAX.csv} contains a list of species along with an unique-per-species numerical ID called \texttt{FNFNUM}. The files \texttt{FARMACY.csv} and \texttt{FARMACY\_NEW.csv} provide a mapping to substances by referencing the aforementioned IDs. Making the lookup functionality operational is effectively parsing these files, scanning \texttt{FNFTAX.csv} for a match in the \texttt{TAXON}-field and returning the \texttt{FNFNUM} of that particular line. In a similar fashion, \texttt{FARMACY.csv} and \texttt{FARMACY\_NEW.csv} can be scanned in order to accumulate any entries that have the previously obtained numer as their \texttt{FNFNUM}. Finally the resulting list must be deduplicated.
\subsection{Resolving Compound Names to Structures}
It is very likely that the next step in the pipeline involves cheminformatical methods like docking simulations or molecular dynamics simulations. These can be easily automated and require little to no human interaction. However these methods do not operate on names of compounds but rather on their threedimensional structure, charges, atom types, e.t.c. In order to feed these simulations the pipeline must be able to convert a compound name into a digital representation of the structure of the molecule. Chemical compounds are textually represented by either a trivial name (e.g. \textit{Hydroquinone}) or a IUPAC-name (e.g \textit{Benzene-1,4-diol}). \\
IUPAC-nomenclature can be considered a declarative language, designed to allow the full reconstruction of the molecular topology from a name. In practice, the package \textit{Open Parser for Systematic IUPAC nomenclature} (OPSIN) \cite{opsin} provides that functionality. As OPSIN itself only provides topological information, additional measures have to be taken in order to obtain structural information. The (CML-)output of a finished OPSIN process is a list of atoms and their respective bonds but the position of every atom is set to (0,0,0). This hinders energetic forcefield algorithms such as MMFF94 \cite{mmff94-1, mmff94-2, mmff94-3, mmff94-4, mmff94-5} to operate correctly, as every atom being in the same spot results in a net-force of 0 on each atom. The pipeline therefore adds \texttt{cmljitter} \footnote{https://notabug.org/grindhold/cmljitter} as a step to randomize the atom positions before applying any means of energetic relaxation. \\
Previous undocumented experiences working with OPSIN and the limitations of it's functionality (e.g. handling E,Z-isomery and polymers), make it a reasonable choice to add a different way to retrieve chemical structures by direct lookups in the Pubchem \cite{pubchem} database. Pubchem is an extensive list of SDF-files that contain two- or threedimensional structures along with several variants of IUPAC names that can be scanned for matches.
The final step of conversion, as far as this pipeline is concerned, is creating conformers for simulations by using \textit{OpenBabel}'s \cite{obabel} \texttt{obminimize} tool.
\begin{Figure}
    \centering
    \digraph[width=\textwidth]{prototype}{
        rankdir=TB;
        inp [label="GPS-Coordinate" fontsize=8]
        geo [label="EEA Geobiological Map" shape=box fontsize=8]
        marine [label="EEA Marinebiological Map" shape=box fontsize=8]
        zonetrans [label="Translation Table" shape=box fontsize=8]
        maes [label="MAES Species database" shape=box fontsize=8]
        drduke [label="Dr. Duke's databases" shape=box fontsize=8]
        opsin [label="OPSIN" shape=box fontsize=8]
        pubchem [label="Pubchem" shape=box fontsize=8]
        cmljitter[label="cmljitter" shape=box fontsize=8]
        obminimize[label="OpenBabel" shape=box fontsize=8]
        out [label="Molecular structure" fontsize=8]

        inp->geo[label="EPSG:4326 coordinate" fontsize=8]
        inp->marine[label="EPSG:4326 coordinate" fontsize=8]
        geo->zonetrans [label="lowercase zone name" fontsize=8]
        marine->zonetrans [label="long zone name" fontsize=8]
        zonetrans->maes [label="zone code" fontsize=8]
        maes->drduke [label="biological name" fontsize=8]
        drduke->opsin [label="IUPAC-name" fontsize=8]
        drduke->pubchem [label="IUPAC-name" fontsize=8]
        opsin->cmljitter [label="cml" fontsize=8]
        cmljitter->obminimize [label="cml" fontsize=8]
        pubchem->obminimize [label="sdf" fontsize=8]
        obminimize->out [label="sdf" fontsize=8]
    }
    \captionof{figure}{Finished Prototype Pipeline Structure}
    \label{fig:prototype_pipeline}
\end{Figure}
\section{Evaluation}
To test the pipeline, the point \texttt{52.4597, 13.2193} which is an arbitrary GPS coordinate inside Berlin, Germany, is being queried. It should result in the pipeline querying the \texttt{CON} geobiological zone whichstands for "Continental" (see Figure~\ref{fig:zone_translation}). The query results in a list of 177 compounds. This number is unrealistically low. In order to identify the shortcomings that cause this problems the results of each step are being analyzed for anomalies. \\
The list of species derived from the \texttt{CON} zone, is only comprised of 1891 distinct species. This number should be higher. Querying the list for \textit{Urtica dioica}, the common nettle, which should be found across the entire european continent shows that it is not contained. This suggests that the species database is either incomplete or focuses on species that are of special interest to the authorities. Doing this research in detail was postponed due to priorites. Manually adding \textit{Urtica dioica} to the list raises the number of results to 253. \\
When inspecting the resulting list of compound, it comes to attention that many of the entries are simple elements which are not as interesting for pharmaceutical purposes as organic molecules. These could safely be blacklisted in a future iteration. \\
Considering the origins of the datasources, it does not seem unlikely that Dr. Dukes databases and MAES Species database do not produce the best intersectional data due to their different geographic origins. The U.S. Department of Agriculture is likely more concerned with plants native to North America than plants native to Europe. \\
This section shall be concluded by a list of the number of found compounds mapped to their respective geobiological zones to provide a base to measure improvements made to the pipeline in the future:
\begin{Figure}
    \centering
    \begin{tabular}{|l|l|l|}
    \hline
    Zone & Number of Compounds \\
    \hline
     ALP& 177\\
     ANA& 0\\
     ARC& 0\\
     ATL& 177\\
     BLS& 41\\
     BOR& 76\\
     CON& 177\\
     MAC& 0\\
     MED& 223\\
     OUT& 0\\
     PAN& 41\\
     STE& 41\\
     MATL& 0\\
     MMAC& 0\\
     MBAL& 0\\
     MMED& 0\\
     MBLS& 0\\
    \hline
    \end{tabular}
    \captionof{figure}{Zone query Results}
    \label{fig:zone_query}
\end{Figure}
\section{Conclusion}
This work has proven that a lookup mechanism that resolves geographic coordinates into chemical compound information is feasible but it has also shown that in order for this to work reliably, more data sources must be acquired and existing datasources must be made more comprehensive. In upcoming work I plan to discuss prefiltering and feeding the output to automated docking simulations.
\printbibliography
\end{document}
