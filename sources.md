pubchem:   ftp://ftp.ncbi.nlm.nih.gov/pubchem/Compound/CURRENT-Full/SDF/
proteindatabase:  ftp://ftp.wwpdb.org/pub/pdb/software/rsyncPDB.sh
species_database:  https://eunis.eea.europa.eu/species.jsp
dr duke's phytochemical and ethnobotanical database: https://data.nal.usda.gov/dataset/dr-dukes-phytochemical-and-ethnobotanical-databases
map for georeferences of habitats:  https://www.eionet.europa.eu/etcs/etc-bd/activities/reporting/article-17/docs/biogeo_and_marineregions_march_2013.jpg/image_view_fullscreen

paper: https://science.sciencemag.org/content/early/2020/04/02/science.abb7269/tab-pdf
A highly conserved cryptic epitope in the receptor-binding domains of SARS-CoV-2 and SARS-CoV
    Meng Yuan1,*, Nicholas C. Wu1,*, Xueyong Zhu1, Chang-Chun D. Lee1, Ray T. Y. So2, Huibin Lv2, Chris K. P. Mok2,†, Ian A. Wilson1,3,†
