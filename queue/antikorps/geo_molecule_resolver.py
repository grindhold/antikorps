
import os
import fiona
import csv
import subprocess
import io
import multiprocessing
import tempfile
import hashlib
import time

from shapely.geometry import Point, shape
from pyproj import Proj, transform
from antikorps import PreprocPipelineTask, ANTIKORPS_DIR, ANTIKORPS_DATA

"""
BioGeo-Data
CRS
IGNF:ETRS89LAEA - ETRS89 Lambert Azimutal Equal Area - Projected

Marine map:
CRS
EPSG:3035 - ETRS89-extended / LAEA Europe - Projected
"""

SPECIES_PATH   = os.path.join(ANTIKORPS_DATA,"species_maes.csv")
GEOJSON_PATH   = ANTIKORPS_DATA
GEODATA        = ["biomes_land.geojson", "biomes_marine.geojson"]
TAXON_PATH     = os.path.join(ANTIKORPS_DATA, "FNFTAX.csv")
FARMACY_PATH_1 = os.path.join(ANTIKORPS_DATA, "FARMACY.csv")
FARMACY_PATH_2 = os.path.join(ANTIKORPS_DATA, "FARMACY_NEW.csv")
PUBCHEM_PATH   = "/home/grindhold/pubchem/SDF"
DISCOVERED_MOL = os.path.join(ANTIKORPS_DIR, "already_discovered.txt")
PIPELINE_LOGS  = os.path.join(ANTIKORPS_DIR, "logs")

EXECUTION_TIME = time.time()
LOG = open(os.path.join(PIPELINE_LOGS, "pipeline_{}.log".format(EXECUTION_TIME)),"w")
def log_pipeline(message):
    LOG.write(message+"\n")
    LOG.flush()

def convert_coords(coords):
    """
    This function is now obsolete due to EPSG:4326-converted geojsons
    generated from the shapefiles
    """
    in_projection = Proj('epsg:4326')
    out_projection = Proj('epsg:3035')
    res = transform(in_projection, out_projection, *coords)
    return list(res)

def query_shapefile(shapefile_path, coords):
    """
    return a list of shapes that correspond to the given coordinates
    [ lon , lat ] in EPSG:4326
    """

    if type(shapefile_path) != str:
        raise TypeError("Shapefile path must be string")

    if not (type(coords) == list) or not type(coords == tuple):
        raise TypeError("Coords must be tuple or list")

    if len(coords) != 2:
        raise ValueError("Coords must have length of 2")

    for i, c in enumerate(coords):
        if type(c) not in (int, float):
            raise TypeError("Coord[{}] must have type int or float")

    shapefile = fiona.open(shapefile_path)
    point = Point(*coords)
    selected_shapes = []

    for shp in shapefile:
        qshp = shape(shp['geometry'])
        if point.buffer(0.001).intersects(qshp):
            selected_shapes.append(shp)

    return selected_shapes

def biome_region_to_pre2012(geofile, name):
    translation = {
        "biomes_land.geojson": {
            'alpine': 'ALP',
            'anatolian': 'ANA',
            'arctic': 'ARC',
            'atlantic': 'ATL',
            'blackSea': 'BLS',
            'boreal': 'BOR',
            'continental' : 'CON',
            'macaronesia': 'MAC',
            'mediterranean': 'MED',
            'outside': 'OUT',
            'pannonian': 'PAN',
            'steppic': 'STE'
        },
        "biomes_marine.geojson": {
            'Noth-east Atlantic Ocean': 'MATL',
            'Macaronesia': 'MMAC',
            'Greater North Sea, incl. the Kattegat and the English Channel': 'MATL',
            'Celtic Seas': 'MATL',

            'Baltic Sea': 'MBAL',

            'Ionian Sea and the Central Mediterranean Sea' : 'MMED',
            'Western Mediterranean Sea' : 'MMED',
            'Adriatic  Sea' : 'MMED',
            'Aegean-Levantine Sea' : 'MMED',

            'Black Sea': 'MBLS',
        }
    }
    return translation[geofile][name]

def get_species_from_habitat(*habitats):
    for habitat in habitats:
        if type(habitat) != str:
            raise TypeError("All habitats must be given as pre2012 code [string]")
    
    species = csv.reader(open(SPECIES_PATH))
    res = []
    for s in species:
        if s[1] in habitats:
            res.append(s)

    log_pipeline("Found {} species in habitat(s): {}".format(len(res), habitats))
    return res

def get_compounds_from_species(species):
    species = [s[2].lower() for s in species]
    farmacy_taxon = csv.reader(open(TAXON_PATH))

    species_ids = {}
    for t in farmacy_taxon:
        if t[1].lower() in species:
            species_ids[t[0]] = 1

    compounds = []

    farmacy_1 = csv.reader(open(FARMACY_PATH_1))
    for f in farmacy_1:
        if species_ids.get(f[0]):
            compounds.append(f)

    old_len = len(compounds)
    log_pipeline("Found {} compounds in {} species in FARMACY (old)".format(old_len, len(species)))

    farmacy_2 = csv.reader(open(FARMACY_PATH_2))
    for f in farmacy_1:
        if species_ids.get(f[0]):
            compounds.append(f)
    log_pipeline("Found {} compounds in {} species in FARMACY (new)".format(len(compounds)-old_len, len(species)))
        
    return [c[1] for c in compounds]

def get_species_from_compound_in_biom(compound, biom):
    #TODO: implement
    pass
 
def query_habitat(coords):
    res = []
    for gj in GEODATA:
        for shape in query_shapefile(os.path.join(GEOJSON_PATH, gj), coords):
            if gj == "biomes_land.geojson":
                res.append(biome_region_to_pre2012(gj, shape['properties']['short_name']))
            elif gj == "biomes_marine.geojson":
                res.append(biome_region_to_pre2012(gj, shape['properties']['Sub_region']))
    return res

def get_compounds_from_biome(*habitat):
    return get_compounds_from_species(get_species_from_habitat(*habitat))

def get_chemicals_from_coords(coords):
    return get_compounds_from_biome(*query_habitat(coords))

def get_structure_via_opsin(name):
    args = ['/usr/bin/java', '-jar', '/usr/share/java/opsin.jar', '-o', 'cml']
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = p.communicate(bytes(name+"\n", 'utf-8'))
    returncode = p.wait()
    output = output.decode('utf-8')
    if not "<atomArray>" in output:
        return None
    else:
        return cml_jitter(output)

def _do_get_structure_via_pubchem_path(args):
    args = ['/usr/local/bin/cq', args[0], args[1]]
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = p.communicate(b'')
    returncode = p.wait()
    output = output.decode('utf-8')
    if output == "":
        return None
    else:
        return output

def get_structure_via_pubchem(*name):
    tmpfile = tempfile.NamedTemporaryFile()
    for n in name:
        tmpfile.write(bytes("{}\n".format(n),'utf-8'))
    tmpfile.flush()
    pool = multiprocessing.Pool(6)
    files = []
    for f in os.listdir(PUBCHEM_PATH):
        if f.endswith(".sdf.gz"):
            files.append(os.path.join(PUBCHEM_PATH,f))
    args = [[path, tmpfile.name] for path in files]
    results = pool.map(_do_get_structure_via_pubchem_path, args)
    pool.close()
    pool.join()
    tmpfile.close()
    print(results)
    return results

def cml_jitter(cml):
    p = subprocess.Popen(['/usr/local/bin/cmljitter'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    output, error = p.communicate(bytes(cml, 'utf-8'))
    returncode = p.wait()
    return output.decode('utf-8')
    

def obabel(inp, intyp, outtyp):
    t = tempfile.NamedTemporaryFile()
    t.write(bytes(inp,'utf-8'))
    t.flush()
    args = ['/usr/bin/obabel', '-i', intyp, t.name, '-o', outtyp]
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #output, error = p.communicate(bytes(inp+"\n", 'utf-8'))
    output, error = p.communicate(b"")
    returncode = p.wait()
    return output.decode('utf-8')

def get_sdf(compounds):
    if type(compounds) != list:
        compounds = [compounds]
    res = []
    for compound in compounds:
        if compound.startswith("<"):
            res.append(obabel(compound, "cml", "sdf"))
        else:
            res.append(compound)
    if len(res) == 1:
        res = res[0]
    return res

def _check_compound_discovered_file():
    if not os.path.exists(DISCOVERED_MOL):
        open(DISCOVERED_MOL,"w").close()

def mark_compound_discovered(compound):
    _check_compound_discovered_file()
    f = open(DISCOVERED_MOL, "a")
    f.write(compound+"\n")
    f.close()

def add_new_molecule_jobs(coords, pathogen):
    compounds = {k: None for k in get_chemicals_from_coords(coords)}

    _check_compound_discovered_file()
    already_discovered = open(DISCOVERED_MOL)
    discovered_compounds = set(already_discovered.read().split("\n"))
    already_discovered.close()
    for compound in compounds.keys():
        if compound in discovered_compounds:
            del(compounds[compound])

    resolved_compounds = {}
    for compound in compounds.keys():
        opsin_solution = get_structure_via_opsin(compound)
        if opsin_solution is not None:
            resolved_compounds[compound] = get_sdf(opsin_solution)

    solved_by_opsin = len(resolved_compounds)
    log_pipeline("Converted {} of {} compounds via libopsin".format(solved_by_opsin, len(compounds)))
    for name, compound in resolved_compounds.items():
        PreprocPipelineTask(name, compound, "6vxx")
        mark_compound_discovered(name)
        del(compounds[name])

    other_compounds = get_structure_via_pubchem(*compounds.keys())
    cq_resolved = 0
    for compound in other_compound:
        if compound is not None:
            cq_resolved += 1
            name = hashlib.md5(bytes(compound, 'utf-8')).hexdigest()
            PreprocPipelineTask(name, compound, pathogen)
    log_pipeline("Converted {} of {} compounds via pubchem".format(len(cq_resolved), len(compounds)))
