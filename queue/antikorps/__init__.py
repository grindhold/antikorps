#!/usr/bin/python3

import os
import subprocess
import threading
import uuid
import json
import time
import traceback
import hashlib
import re
import zmq

ANTIKORPS_DIR  = "/var/antikorps"
RESULTS_DIR = os.path.join(ANTIKORPS_DIR, "results")
JOBS_DIR = os.path.join(ANTIKORPS_DIR, "jobs")
LOG_DIR = os.path.join(ANTIKORPS_DIR, "log")
TASKS_DIR = os.path.join(ANTIKORPS_DIR, "tasks")
ANTIKORPS_DATA = "/usr/local/share/antikorps"

PUBCHEM_DATA = "/home/grindhold/pubchem/SDF"

INTERNAL_TOKEN = str(uuid.uuid4())

class JobException(Exception): pass

class Job(object):
    STATUS_DRAFT = 0
    STATUS_PENDING = 1
    STATUS_PROCESSING = 2
    STATUS_FINISHED = 3

    def __init__(self):
        self._ident = str(uuid.uuid4())
        self._task_id = None
        self._timestamp = str(int(time.time()))
        self._status = self.__class__.STATUS_DRAFT
        self._expected_returncode = 0
        self._executable = "true"
        self._stdout = ""
        self._stderr = ""
        self._params = []
        self._next_job = None
        self._data = {}
        self._failed = False
        self._internal = ""

    def serialize(self):
        return json.dumps(self.__dict__)

    @classmethod
    def hydrate(cls, j):
        o = cls()
        o.__dict__.update(json.loads(j))
        return o

    @property
    def ident(self):
        return self._ident

    @ident.setter
    def ident(self, ident):
        self._ident = ident

    @property
    def task_id(self):
        return self._task_id

    @task_id.setter
    def task_id(self, task_id):
        self._task_id = task_id


    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, timestamp):
        self._timestamp = timestamp

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    @property
    def expected_returncode(self):
        return self._expected_returncode

    @expected_returncode.setter
    def expected_returncode(self, expected_returncode):
        self._expected_returncode = expected_returncode

    @property
    def stdout(self):
        return self._stdout

    @stdout.setter
    def stdout(self, stdout):
        self._stdout = stdout

    @property
    def stderr(self):
        return self._stderr

    @stderr.setter
    def stderr(self, stderr):
        self._stderr = stderr

    @property
    def executable(self):
        return self._executable

    @executable.setter
    def executable(self, executable):
        self._executable = executable

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, params):
        self._params = params

    @property
    def next_job(self):
        return self._next_job

    @next_job.setter
    def next_job(self, job):
        self._next_job = job
        
    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @property
    def failed(self):
        return self._failed

    @failed.setter
    def failed(self, failed):
        self._failed = failed

    @property
    def internal(self):
        return self._internal

    @internal.setter
    def internal(self, internal):
        self._internal = internal

    def run(self):
        args = self.params
        args.insert(0, self.executable)
        result_path = os.path.join(RESULTS_DIR, self.task_id)
        if not os.path.exists(result_path):
            os.makedirs(result_path)
        stdoutfile = open(self.stdout, "w")
        stderrfile = open(self.stderr, "a")
        p = subprocess.Popen(args, stdout=stdoutfile, stderr=stderrfile)
        returncode = p.wait()
        stdoutfile.close()
        stderrfile.close()
        if returncode != self.expected_returncode:
            raise JobException("Wrong returncode")

class Task(object):
    def __init__(self):
        self._ident = str(uuid.uuid4())
        self._name = None
        self._timestamp = None
        self._priority = 1
        self._jobs = []

    @property
    def ident(self):
        return self._ident

    @ident.setter
    def ident(self, ident):
        self._ident = ident

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, timestamp):
        self._timestamp = timestamp

    @property
    def priority(self):
        return self._priority

    @priority.setter
    def priority(self, priority):
        self._priority = priority

    @property
    def jobs(self):
        return self._jobs

    @jobs.setter
    def jobs(self, jobs):
        self._jobs = jobs

    def add_job(self, target_out, target_err, executable, *args, **kwargs):
        j = Job()
        j.executable = executable
        j.stdout = target_out
        j.stderr = target_err
        j.params = list(args)
        j.task_id = self._ident
        j.data = kwargs.get("data", {})
        self._jobs.append(j)
        return j

    def get_workdir(self):
        return os.path.join(ANTIKORPS_DIR, "results", self.ident)

    def create_workdir(self):
        workdirpath = self.get_workdir()
        if not os.path.exists(workdirpath):
            os.makedirs(workdirpath, exist_ok=True)
        return workdirpath

    def deploy(self, address="127.0.0.1", port=1510):
        ømq = zmq.Context()
        snd = ømq.socket(zmq.PUSH)
        snd.connect("tcp://{}:{}".format(address, port))
        for i, j in enumerate(self.jobs):
            print("[T] {} {}/{} attempting to deploy…".format(j.ident, i+1, len(self.jobs)))
            try:
                snd.send_string(j.serialize())
            except Exception as e :
                print("[T] {} {}/{} failed to deploy…".format(j.ident, i+1, len(self.jobs)))
                break
        print("[T] Deployed successfully as task {}".format(self.ident))


class Worker(threading.Thread):
    """Thread executing tasks from a given tasks queue"""
    def __init__(self):
        threading.Thread.__init__(self)
        self.logger = Logger()
        self.ømq = zmq.Context()
        self.rcv = self.ømq.socket(zmq.PULL)
        self.rcv.connect("tcp://localhost:1511")
        self.snd = self.ømq.socket(zmq.PUSH)
        self.snd.connect("tcp://localhost:1512")
        self.daemon = True
        self.start()

    def run(self):
        while True:
            jsondata = self.rcv.recv_string()
            j = Job.hydrate(jsondata)
            self.logger("[W] {} {} Commence processing".format(threading.get_ident(),j.ident))
            try:
                self.logger("[W] {} {} $ {}".format(threading.get_ident(),j.ident, [j.executable, *j.params, j.stdout, j.stderr]))
                j.run()
            except JobException as ex:
                j.failed = True
                self.logger("[W] {} {} Job did not receive expected returncode.".format(threading.get_ident(), j.ident))
            except Exception as ex:
                j.failed = True
                self.logger("[W] {} {} Executional error occured ".format(threading.get_ident(), j.ident))
                self.logger("".join(traceback.TracebackException.from_exception(ex).format()) == traceback.format_exc() == "".join(traceback.format_exception(type(ex), ex, ex.__traceback__)))
                self.logger("".join(traceback.TracebackException.from_exception(ex).format()))
            self.logger("[W] {} {} Finished processing".format(threading.get_ident(), j.ident))
            self.snd.send_string(j.serialize())

class Arbiter(threading.Thread):
    """Arbitrates jobs to workers"""
    def __init__(self):
        threading.Thread.__init__(self)
        self.logger = Logger()
        self.ømq = zmq.Context();
        self.snd = self.ømq.socket(zmq.PUSH)
        self.snd.bind("tcp://127.0.0.1:1511")
        self.rcv = self.ømq.socket(zmq.PULL)
        self.rcv.bind("tcp://127.0.0.1:1510")
        self.daemon = True
        self.start()

    def run(self):
        while True:
            jsondata = self.rcv.recv_string()
            try:
                j = Job.hydrate(jsondata)
                if not re.match("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", j.ident):
                    self.logger("[A] {} {}[…] is not a valid job id. Aborting.".format(threading.get_ident(), j.ident[:32]))
                    continue
                if not re.match("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", j.task_id):
                    self.logger("[A] {} {}[…] is not a valid task id. Aborting.".format(threading.get_ident(), j.task_id[:32]))
                    continue
            except Exception as e:
                self.logger("[A] {} Could not decode job. Aborting: {}".format(threading.get_ident(), e))
            else:
                taskpath = os.path.join(JOBS_DIR, j.task_id)
                if j.internal == INTERNAL_TOKEN:
                    nextjobfile = open(os.path.join(taskpath, "{}.json".format(j.next_job)))
                    nextjobdata = nextjobfile.read()
                    nextjobfile.close()
                    try:
                        nextjob = Job.hydrate(nextjobdata)
                    except Exception as e:
                        self.logger("[A] {} {} could not read next job {}.".format(threading.get_ident(), j.ident, j.next_job))
                        continue
                    self.snd.send_string(nextjobdata)
                    self.logger("[A] {} {} enqueued.".format(threading.get_ident(), nextjob.ident))
                    continue

                start_job = False
                self.logger("[A] {} {} persisting job info.".format(threading.get_ident(), j.ident))
                if not os.path.exists(taskpath):
                    start_job = True
                    os.makedirs(taskpath)
                jobfile = open(os.path.join(taskpath, "{}.json".format(j.ident)), "w")
                jobfile.write(jsondata)
                jobfile.close()
                self.logger("[A] {} {} persisting job info successful.".format(threading.get_ident(), j.ident))
                if j.data != {} and type(j.data) == dict:
                    workdir = os.path.join(RESULTS_DIR, j.task_id)
                    if not os.path.exists(workdir):
                        os.makedirs(workdir)
                    for n, d in j.data.items():
                        self.logger("[A] {} {} persisting resource {}.".format(threading.get_ident(), j.ident, n))
                        try:
                            f = open(os.path.join(workdir, n), "w")
                            f.write(d)
                            f.close()
                        except Exception as e:
                            self.logger("[A] {} {} could not persist {}.".format(threading.get_ident(), j.ident, n))
                            self.logger("[A] {} {} Could not save resource. Aborting: {}".format(threading.get_ident(), j.ident, e))
                            continue

                if start_job:
                    self.snd.send_string(jsondata)
                    self.logger("[A] {} {} enqueued.".format(threading.get_ident(), j.ident))

class Collector(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.logger = Logger()
        self.ømq = zmq.Context()
        self.rcv = self.ømq.socket(zmq.PULL)
        self.rcv.bind("tcp://127.0.0.1:1512")
        self.snd = self.ømq.socket(zmq.PUSH)
        self.snd.connect("tcp://127.0.0.1:1510")
        self.daemon = True
        self.start()

    def run(self):
        while True:
            jsondata = self.rcv.recv_string()
            j = Job.hydrate(jsondata)
            if not j.failed:
                self.logger("[C] {} {} returned successfully.".format(threading.get_ident(), j.ident))
                taskpath = os.path.join(JOBS_DIR, j.task_id)
                os.unlink(os.path.join(taskpath, "{}.json".format(j.ident)))
                self.logger("[C] {} {} removed jobfile.".format(threading.get_ident(), j.ident))
                if j.next_job is not None:
                    j.internal = INTERNAL_TOKEN
                    self.logger("[C] {} {} notifying arbiter to deploy next job {}.".format(threading.get_ident(), j.ident, j.next_job))
                    self.snd.send_string(j.serialize())

            else:
                self.logger("[C] {} {} returned unsuccessfully.".format(threading.get_ident(), j.ident))

class Logger(object):
    def __init__(self):
        self.ømq = zmq.Context()
        self.snd = self.ømq.socket(zmq.PUSH)
        self.snd.connect("tcp://127.0.0.1:1513")

    def __call__(self, message):
        self.snd.send_json({
            "internal": INTERNAL_TOKEN,
            "message": str(message)})

class Logservice(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.ømq = zmq.Context()
        self.rcv = self.ømq.socket(zmq.PULL)
        self.rcv.bind("tcp://127.0.0.1:1513")
        self.daemon = True
        self.start()
        if not os.path.exists(LOG_DIR):
            os.makedirs(LOG_DIR)
        self.logfile = open(os.path.join(LOG_DIR, "antikorps.log"), "a")

    def run(self):
        while True:
            try:
                msg = self.rcv.recv_json()
            except Exception as e:
                continue
            if msg["internal"] == INTERNAL_TOKEN:
                msg = msg["message"]
                self.logfile.write(msg + "\n")
                self.logfile.flush()


class Daemon(object):
    def __init__(self, arbiter=True, n_workers=2):
        workers = [Worker() for _ in range(n_workers)]
        if arbiter:
            a = Arbiter()
            c = Collector()
            l = Logservice()
            a.join()
            c.join()
            l.join()
        [w.join() for w in workers]


class PubchemTask(Task):
    def __init__(self, sdffile, pathogen):
        Task.__init__(self)
        # TODO: make os-independent by doing tempfile shit with tempfile module
        # "/home/grindhold/pubchem/SDF/firstfile/{}.sdf"
        self.name = os.path.basename(sdffile).rstrip(".sdf")
        workdir = self.create_workdir()
        logpath = os.path.join(workdir, "{}.log".format(self.__class__.__name__))
        self.add_job("/tmp/conversion_{}.pdb".format(self.name), logpath,
                     "obabel", "-i", "sdf", sdffile, "-o", "pdb")
        self.add_job("/tmp/conversion_opt_{}.pdb".format(self.name), logpath,
                     "obconformer", "10000", "1", "/tmp/conversion_{}.pdb".format(self.name))
        ligandpath = os.path.join(workdir, "{}.pdbqt".format(self.name))
        self.add_job(logpath, logpath,
                     "pdb2pdbqt", "/tmp/conversion_opt_{}.pdb".format(self.name), ligandpath)
        configpath = os.path.join(ANTIKORPS_DIR, "pathogens", "{}.conf".format(pathogen))
        self.add_job(logpath, logpath,
                     "vina", "--config", configpath, "--ligand", ligandpath)
        self.timestamp = str(int(time.time()))
        for i, job in enumerate(self.jobs):
            job.status = Job.STATUS_PENDING
            if i > 0:
                jobs[i-1].next_job = job.ident
        self.deploy()

class PreprocPipelineTask(Task):
    def __init__(self, name, sdfdata, pathogen):
        Task.__init__(self)
        self.name = "{}_{}_geoloc".format(hashlib.md5(bytes(name,'utf-8')).hexdigest(), pathogen)
        self.priority = 2

        workdir = self.create_workdir()

        sdfpath = os.path.join(workdir, "compound.sdf")
        sdffile = open(sdfpath, "w")
        sdffile.write(sdfdata)
        sdffile.close()

        namefilepath = os.path.join(workdir, "nameinfo.txt")
        namefile = open(namefilepath, "w")
        namefile.write(name)
        namefile.close()

        logpath = os.path.join(workdir, "{}.log".format(self.__class__.__name__))
        self.add_job("/tmp/conversion_{}.pdb".format(self.name), logpath,
                     "obabel", "-i", "sdf", sdfpath, "-o", "pdb")
        self.add_job("/tmp/conversion_opt_{}.pdb".format(self.name), logpath,
                     "obconformer", "1", "20", "/tmp/conversion_{}.pdb".format(self.name))
        ligandpath = os.path.join(workdir, "{}.pdbqt".format(self.name))
        self.add_job(logpath, logpath,
                     "pdb2pdbqt", "/tmp/conversion_opt_{}.pdb".format(self.name), ligandpath)
        configpath = os.path.join(ANTIKORPS_DIR, "pathogens", "{}.conf".format(pathogen))
        self.add_job(logpath, logpath,
                     "vina", "--config", configpath, "--ligand", ligandpath)
        self.timestamp = str(int(time.time()))
        for i, job in enumerate(self.jobs):
            job.status = Job.STATUS_PENDING
            if i > 0:
                jobs[i-1].next_job = job.ident
        self.deploy()
