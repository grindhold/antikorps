#!/usr/bin/python3

from distutils.core import setup

setup(
    name="antikorps",
    version="0.1",
    description="Queue managing molecular docking tasks",
    author="Daniel 'grindhold' Brendle",
    author_email="grindhold+antikorps@skarphed.org",
    packages = [
        'antikorps',
    ],
    scripts = ['bin/pubchem_enqueue', 'bin/antikorps', 'bin/affinity_ranking', 'bin/validation_ranking']
)
