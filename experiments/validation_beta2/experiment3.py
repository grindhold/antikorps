import sys
import os
import time
import hashlib
from antikorps import Task, ANTIKORPS_DIR, Job

class ValidationAdrb2Task(Task):
    NAME_PATTERN = "adrb2_validation_{}"
    def __init__(self, name, sdfdata):
        Task.__init__(self)
        pathogen = "adrb2"
        self.name = self.__class__.NAME_PATTERN.format(hashlib.md5(bytes(name,'utf-8')).hexdigest(), pathogen)
        self.priority = 3 
        self.persist()

        workdir = self.create_workdir()

        sdfpath = os.path.join(workdir, "compound.sdf")
        sdffile = open(sdfpath, "w")
        sdffile.write(sdfdata)
        sdffile.close()

        namefilepath = os.path.join(workdir, "nameinfo.txt")
        namefile = open(namefilepath, "w")
        namefile.write(name)
        namefile.close()

        jobs = []
        logpath = os.path.join(workdir, "{}.log".format(self.__class__.__name__))
        jobs.append(self.add_job(0, "/tmp/conversion_{}.pdb".format(self.name), logpath,
                     "obabel", "-i", "sdf", sdfpath, "-o", "pdb"))
        jobs.append(self.add_job(1, "/tmp/conversion_opt_{}.pdb".format(self.name), logpath,
                     "obconformer", "1", "20", "/tmp/conversion_{}.pdb".format(self.name)))
        ligandpath = os.path.join(workdir, "{}.pdbqt".format(self.name))
        jobs.append(self.add_job(2, logpath, logpath,
                     "pdb2pdbqt", "/tmp/conversion_opt_{}.pdb".format(self.name), ligandpath))
        configpath = os.path.join(ANTIKORPS_DIR, "pathogens", "{}.conf".format(pathogen))
        jobs.append(self.add_job(3, logpath, logpath,
                     "vina", "--config", configpath, "--ligand", ligandpath))
        self.timestamp = str(int(time.time()))
        self.persist()
        for i, job in enumerate(jobs):
            job.status = Job.STATUS_PENDING
            if i > 0:
                jobs[i-1].next_job = job
        for job in jobs:
            job.persist()

    @staticmethod
    def get_affinities(path):
        f = open(path)
        lc = 0
        start = None 
        ret = []
        for l in f.read().split("\n"):
            if start is None and l.startswith("mode |"):
                start = lc
            elif start is not None:
                if l[0] == "W":
                    break
                if lc - start > 2:
                    data = l.split()
                    ret.append((int(data[0]),float(data[1])))
            lc += 1
        return ret

    @classmethod
    def _get_experiment_folders(cls):
        pattern = getattr(cls, "NAME_PATTERN", None)
        if pattern is None:
            raise ValueError("The class {} has no NAME_PATTERN attribute. It's manadtory to resolve the folders.".format(cls.__name__))
        pattern = pattern.replace("{}","")
        folders = []
        for d in os.listdir(os.path.join(ANTIKORPS_DIR, "results")):
            if d.startswith(pattern):
                folders.append(d)
        return folders
        
    @classmethod
    def analysis(cls):
        results = []
        for f in cls._get_experiment_folders():
            nameinfo = open(os.path.join(ANTIKORPS_DIR, "results", f, "nameinfo.txt")).read()
            results.append((nameinfo, cls.get_affinities(os.path.join(ANTIKORPS_DIR, "results", f, cls.__name__+".log"))))
        print("Candidate_Number,compound [PUBCHEM/NAME],Mode,Affinity [kcal / mol]")
        candidate_nr = 0
        for r in results:
            for m, v in r[1]:
                print("{},{},{},{}".format(candidate_nr, r[0], m, v))
            candidate_nr += 1

HELPTEXT = """

Available operations:
    deploy   → enqueue and start the experiment
    analyze  → get the data from the experiment as CSV file
"""
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(HELPTEXT)
        sys.exit(1)
    if sys.argv[1] == "deploy":
        for f in os.listdir(os.path.dirname(os.path.abspath(__file__))):
            if not f.endswith(".sdf"):
                continue
            sdff = open(f)
            sdfdata = sdff.read()
            sdff.close()
            ValidationAdrb2Task(f.replace(".sdf",""),sdfdata) 
    elif sys.argv[1] == "analyze":
        ValidationAdrb2Task.analysis()
