import os
import hashlib
import time
from antikorps import Task, ANTIKORPS_DIR, Job

class ValidationAdrb2Task(Task):
    def __init__(self, name, sdfdata):
        Task.__init__(self)
        pathogen = "adrb2"
        self.name = "adrb2_validation_{}".format(hashlib.md5(bytes(name,'utf-8')).hexdigest(), pathogen)
        self.priority = 3 
        self.persist()

        workdir = self.create_workdir()

        sdfpath = os.path.join(workdir, "compound.sdf")
        sdffile = open(sdfpath, "w")
        sdffile.write(sdfdata)
        sdffile.close()

        namefilepath = os.path.join(workdir, "nameinfo.txt")
        namefile = open(namefilepath, "w")
        namefile.write(name)
        namefile.close()

        jobs = []
        logpath = os.path.join(workdir, "{}.log".format(self.__class__.__name__))
        jobs.append(self.add_job(0, "/tmp/conversion_{}.pdb".format(self.name), logpath,
                     "obabel", "-i", "sdf", sdfpath, "-o", "pdb"))
        jobs.append(self.add_job(1, "/tmp/conversion_opt_{}.pdb".format(self.name), logpath,
                     "obconformer", "1", "20", "/tmp/conversion_{}.pdb".format(self.name)))
        ligandpath = os.path.join(workdir, "{}.pdbqt".format(self.name))
        jobs.append(self.add_job(2, logpath, logpath,
                     "pdb2pdbqt", "/tmp/conversion_opt_{}.pdb".format(self.name), ligandpath))
        configpath = os.path.join(ANTIKORPS_DIR, "pathogens", "{}.conf".format(pathogen))
        jobs.append(self.add_job(3, logpath, logpath,
                     "vina", "--config", configpath, "--ligand", ligandpath))
        self.timestamp = str(int(time.time()))
        self.persist()
        for i, job in enumerate(jobs):
            job.status = Job.STATUS_PENDING
            if i > 0:
                jobs[i-1].next_job = job
        for job in jobs:
            job.persist()

if __name__ == "__main__":
    for f in os.listdir(os.path.dirname(os.path.abspath(__file__))):
        if not f.endswith(".sdf"):
            continue
        sdff = open(f)
        sdfdata = sdff.read()
        sdff.close()
        ValidationAdrb2Task(f.replace(".sdf",""),sdfdata) 
