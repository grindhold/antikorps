import sys
import os
import hashlib
import time
import math
import statistics
import random
import zmq

from antikorps import Task, ANTIKORPS_DIR, Job, PUBCHEM_DATA

class ValidationAceTask(Task):
    # number of random molecules to test against for comparison
    N_MOLECULES = 97
    NAME_PATTERN = "ace_validation_{}"
    def __init__(self, name, sdfdata):
        Task.__init__(self)
        pathogen = "1o8a"
        self.name = self.__class__.NAME_PATTERN.format(hashlib.md5(bytes(name,'utf-8')).hexdigest(), pathogen)
        self.priority = 3 

        workdir = self.get_workdir()

        data = {
            "compound.sdf": sdfdata,
            "nameinfo.txt": name
        }


        logpath = os.path.join(workdir, "{}.log".format(self.__class__.__name__))
        self.add_job( "/tmp/conversion_{}.pdb".format(self.name), logpath,
                     "obabel", "-i", "sdf", os.path.join(workdir, "compound.sdf"), "-o", "pdb", data=data)
        self.add_job( "/tmp/conversion_opt_{}.pdb".format(self.name), logpath,
                     "obconformer", "1", "20", "/tmp/conversion_{}.pdb".format(self.name))
        ligandpath = os.path.join(workdir, "{}.pdbqt".format(self.name))
        self.add_job(logpath, logpath,
                     "pdb2pdbqt", "/tmp/conversion_opt_{}.pdb".format(self.name), ligandpath)
        configpath = os.path.join(ANTIKORPS_DIR, "pathogens", "{}.conf".format(pathogen))
        self.add_job(logpath, logpath,
                     "vina", "--config", configpath, "--ligand", ligandpath)
        self.timestamp = str(int(time.time()))
        for i, job in enumerate(self.jobs):
            job.status = Job.STATUS_PENDING
            if i > 0:
                self.jobs[i-1].next_job = job.ident
        self.deploy()

    @classmethod
    def prepare(cls):
        found_molecules = 0
        used_molecules = set()
        while found_molecules < cls.N_MOLECULES:
            pubchem_id = random.randint(0,99999)
            if pubchem_id in used_molecules:
                continue
            used_molecules.add(pubchem_id)
            try:
                sdffile = open(os.path.join(PUBCHEM_DATA, "firstfile", "{}.sdf".format(pubchem_id)))
            except FileNotFoundError:
                continue
            sdfdata = sdffile.read().split("\n")
            sdffile.close()
            exotic_atom_found = False
            for line in sdfdata[4:]:
                fields = line.split()
                if len(fields) != 16:
                    break
                if fields[3] not in ('C','O','H','N','S','F','Cl','Br','I'):
                    exotic_atom_found = True
                    break
            if exotic_atom_found:
                continue
            target = open(os.path.join(os.path.dirname(__file__), "rnd_{}.sdf".format(pubchem_id)), "w")
            target.write("\n".join(sdfdata))
            target.close()
            found_molecules += 1

    @staticmethod
    def get_affinities(path):
        f = open(path)
        lc = 0
        start = None 
        ret = []
        for l in f.read().split("\n"):
            if start is None and l.startswith("mode |"):
                start = lc
            elif start is not None:
                if l[0] == "W":
                    break
                if lc - start > 2:
                    data = l.split()
                    ret.append((int(data[0]),float(data[1])))
            lc += 1
        return ret

    @staticmethod
    def get_proximities(path, i):
        X = 0
        Y = 1
        Z = 2

        f = open(path)
        start = None 
        ret = []
        distances = []
        max_vector = (sys.float_info.min,sys.float_info.min,sys.float_info.min)
        min_vector = (sys.float_info.max,sys.float_info.max,sys.float_info.max)
        for l in f.read().split("\n"):
            if l.startswith("ENDMDL"):
                med = statistics.median(distances)
                avg = sum(distances) / len(distances)
                env = math.sqrt(math.pow(max_vector[X]-min_vector[X], 2) + 
                                math.pow(max_vector[Y]-min_vector[Y], 2) +
                                math.pow(max_vector[Z]-min_vector[Z], 2))
                ret.append((avg,med,env))
                distances = []
            elif l.startswith("HETATM"):
                fields = l.split()
                x, y, z = float(fields[5]), float(fields[6]), float(fields[7])
                distances.append(math.sqrt(math.pow(i[X]-x,2) + math.pow(i[Y]-y, 2) + math.pow(i[Z]-z, 2)))
                max_vector = (max(max_vector[X], x),max(max_vector[Y], y),max(max_vector[Z], z))
                min_vector = (min(min_vector[X], x),min(min_vector[Y], y),min(min_vector[Z], z))
        return ret


    @classmethod
    def _get_experiment_folders(cls):
        pattern = getattr(cls, "NAME_PATTERN", None)
        if pattern is None:
            raise ValueError("The class {} has no NAME_PATTERN attribute. It's manadtory to resolve the folders.".format(cls.__name__))
        pattern = pattern.replace("{}","")
        folders = []
        for d in os.listdir(os.path.join(ANTIKORPS_DIR, "results")):
            if d.startswith(pattern):
                folders.append(d)
        return folders

    @classmethod
    def analysis(cls):
        results = []
        results_prox = []
        i = (36.832470,47.561806,53.201469) # ideal binding site

        for f in cls._get_experiment_folders():
            nameinfo = open(os.path.join(ANTIKORPS_DIR, "results", f, "nameinfo.txt")).read()
            try:
                results.append((nameinfo,
                                cls.get_affinities(os.path.join(ANTIKORPS_DIR, "results", f, cls.__name__+".log")),
                                cls.get_proximities(os.path.join(ANTIKORPS_DIR, "results", f, f+"_out.pdbqt"),i) 
                                ))
            except FileNotFoundError as e:
                results.append((nameinfo, None, None))

        print("Candidate_Number,compound [PUBCHEM/NAME],hash,Mode,Affinity [kcal / mol],d_avg[Â],d_med [Â],r_env [Â]")
        candidate_nr = 0
        for r in results:
            found = False
            for mode in range(min(len(r[1] or []), len(r[2] or []))):
                found = True
                v = r[1][mode][1]
                ideal_distance_avg          = r[2][mode][0]
                ideal_distance_med          = r[2][mode][1]
                molecule_envelope_radius    = r[2][mode][2]
                print("{},{},{},{},{},{},{},{}".format(
                    candidate_nr, r[0],
                    hashlib.md5(bytes(r[0],'utf-8')).hexdigest(),
                    mode,
                    v,
                    ideal_distance_avg,
                    ideal_distance_med,
                    molecule_envelope_radius
                ))
            #if not found:
            #    for i in range(6):
            #        pass#print("{},{},{},NULL,NULL,NULL,NULL".format(candidate_nr, r[0], i))
            candidate_nr += 1

HELPTEXT = """

Available operations:
    deploy   → enqueue and start the experiment
    analyze  → get the data from the experiment as CSV file
"""
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(HELPTEXT)
        sys.exit(1)
    if sys.argv[1] == "deploy":
        for f in os.listdir(os.path.dirname(os.path.abspath(__file__))):
            if not f.endswith(".sdf"):
                continue
            sdff = open(os.path.join(os.path.dirname(__file__),f))
            sdfdata = sdff.read()
            sdff.close()
            ValidationAceTask(f.replace(".sdf",""),sdfdata) 
    elif sys.argv[1] == "analyze":
        ValidationAceTask.analysis()
    elif sys.argv[1] == "prepare":
        ValidationAceTask.prepare()
