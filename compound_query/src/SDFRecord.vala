using GLib;

namespace CompoundQuery {
    const string SDF_DELIMITER = "$$$$";

    class SDFRecord {
        public string full_text { get; private set; }
        public bool is_filtered { get; private set; default = false; }

        public static SDFRecord? new_from_input_stream(DataInputStream is, string[]? filter_terms = null) throws Error {
            string full_text = "";

            string line;
            while ((line = is.read_line(null)) != null) {
                full_text += line + "\n";

                if (line == SDF_DELIMITER) {
                    break;
                }
            }

            // got full record
            if (line == SDF_DELIMITER) {
                var record = new SDFRecord(full_text);

                if (filter_terms.length > 0) {
                    record.filter(filter_terms);
                }

                return record;
            }

            return null;
        }

        protected void filter(string[] terms) {
            this.is_filtered = this.matches(terms);
        }

        private SDFRecord(string full_text) {
            this.full_text = full_text;
        }

        public bool matches(string[] terms) {
            bool matches = false;

            foreach (string term in terms) {
                matches = matches || this.matches_term(term);
            }

            return matches;
        }

        /**
         * Match the full text against a search term
         *
         * Search strategy is to try the lower cased term first
         * as the base use case is to match against IUPAC names
         * which are case insensitive.
         */
        private bool matches_term(string term) {
            if (term.down().match_string(this.full_text, true)) {
                return true;
            }

            if (term.match_string(this.full_text, true)) {
                return true;
            }

            return false;
        }
    }
}
