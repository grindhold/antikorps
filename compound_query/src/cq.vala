using GLib;

namespace CompoundQuery {
    class CompoundQuery {
        private string archive_filename;
        private string[] terms;

        public CompoundQuery(string archive_filename, string[] terms) {
            this.archive_filename = archive_filename;
            this.terms = terms;
        }

        public static int main(string[] argv) {
            // todo: proper args parser
            if (argv.length < 3) {
                stdout.printf(
                "cq - compound query\n\n"
                    + "usage:\n\n"
                    + "\tcq compound_archive.sdf.gz searchterm [searchterm...]\n"
                );
                return 0;
            }

            string archive_filename = argv[1];
            string[] terms = new string[0];

            for (int i = 2; i < argv.length; i++) {
                terms += argv[i];
            }

            if (terms.length == 1 && FileUtils.test(terms[0], FileTest.EXISTS)) {
                try {
                    var termsfile = File.new_for_commandline_arg(terms[0]);
                    var is = new DataInputStream(termsfile.read());
                    terms = new string[0];

                    string term;
                    while ((term = is.read_line(null)) != null) {
                        terms += term;
                    }
                } catch (Error e) {
                    stderr.printf("Failed to read terms from file %s", terms[0]);
                    return 1;
                }
            }

            var cq = new CompoundQuery(archive_filename, terms);

            return cq.search();
        }

        public int search() {
            if (!this.archive_exists()) {
                stderr.printf("Archive file not found\n");
                return 1;
            }

            try {
                File archive = File.new_for_commandline_arg(this.archive_filename);
                var cs = new ConverterInputStream(archive.read(), new ZlibDecompressor(ZlibCompressorFormat.GZIP));
                var is = new DataInputStream(cs);

                SDFRecord record;
                int total = 0;
                int matched = 0;

                while ((record = SDFRecord.new_from_input_stream(is, this.terms)) != null) {
                    if (record.is_filtered) {
                    // if (record.matches(this.terms)) {
                        stdout.printf("%s", record.full_text);
                        matched++;
                    }

                    total++;
                }

                stderr.printf("total records: %d\nmatched records: %d\n", total, matched);
            } catch (Error e) {
                stderr.printf("%s\n", e.message);
            }

            return 0;
        }

        bool archive_exists() {
            return FileUtils.test(this.archive_filename, FileTest.EXISTS);
        }
    }
}
